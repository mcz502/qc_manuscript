%% Get data
mydir    = '/Users/psyc1586_admin/r_dpuk/QC_classifier_additionalMeasures/Mixed_data';
run_dir  = 'outs_svm_rf_corrected';
data_dir = fullfile(mydir,run_dir);

load(fullfile(data_dir,'finalMDL_allVars.mat'),'perf_rf_tab','perf_svm_tab')

%% For RF
temp_rf_perf = table2array(perf_rf_tab);
rf_perf      = temp_rf_perf(:,2:end);
col_map      = [0,0.4470,0.7410; 0.8500,0.3250,0.0980; 0.9290,0.6940,0.1250; 0.4940,0.1840,0.5560; 0.4660,0.6740,0.1880; 0.6350,0.0780,0.1840];
subplot(2,1,1)
for ii = 1:6
    plot(rf_perf(:,ii),'color',col_map(ii,:))
    hold on
end
[max_value,index]=max(rf_perf(:,1));
plot(index,max_value,'b*')
grid('on')
box('off')
legend({'Accuracy','Sensitivity','Specificity','BalAcc','Precision','F1Score','Max Accuracy'},'Location','bestoutside')
xticks([1:1:11])
xticklabels({'10','20','30','40','50',...
             '60','70','80','90','100','All'});
xlabel('Feature size (Ranked)')
ylabel('% performance')
title('Random forest classifier performance')
ax = gca;
ax.FontSize = 6;

%% For SVM
subplot(2,1,2)
temp_svm_perf = table2array(perf_svm_tab);
svm_perf      = temp_svm_perf(:,2:end);
col_map      = [0,0.4470,0.7410; 0.8500,0.3250,0.0980; 0.9290,0.6940,0.1250; 0.4940,0.1840,0.5560; 0.4660,0.6740,0.1880; 0.6350,0.0780,0.1840];
for ii = 1:6
    plot(svm_perf(:,ii),'color',col_map(ii,:))
    hold on
end
[max_value,index]=max(svm_perf(:,1));
plot(index,max_value,'b*')
legend({'Accuracy','Sensitivity','Specificity','BalAcc','Precision','F1Score','Max Accuracy'},'Location','bestoutside')
xticks([1:1:11])
xticklabels({'10','20','30','40','50',...
             '60','70','80','90','100','All'});
grid('on')
box('off')
xlabel('Feature size (Ranked)')
ylabel('% performance')
title('Linear SVM classifier performance')
ax = gca;
ax.FontSize = 6;

%% Save

saveas(gcf,fullfile(data_dir,'perf_plot_testdata.fig'))
print(fullfile(data_dir,'perf_plot_testdata'),'-dpng','-r600')