load('/Users/psyc1586_admin/r_Sch/QC_classifier/Mixed_data/feature_names.mat');

final_ranked = all_features;

if length(final_ranked) ~= length(featurenames)
    error('\n feature names and ranked feature length mismatched /n');
else
    n_features = length(final_ranked);
end


for ii = 1:15
    subplot(8,2,ii)
    Y            = n_features:-1:1;
    h            = bar(final_ranked(:,ii),Y); 
    h.FaceColor  = 'flat';
    colorData    = h.CData; 
    
    for i = 1:n_features
        if     i == 1
            h.CData(final_ranked(i,ii),:) = [179 88 6]/256;
        elseif i == 2
            h.CData(final_ranked(i,ii),:) = [224 130 20]/256;
        elseif i == 3
            h.CData(final_ranked(i,ii),:) = [253 184 99]/256;
        elseif i == 4
            h.CData(final_ranked(i,ii),:) = [254 224 182]/256;
        elseif i == 5
            h.CData(final_ranked(i,ii),:) = [53 151 143]/256;
        elseif i == 6
            h.CData(final_ranked(i,ii),:) = [215 48 39]/256;
        elseif i == 7
            h.CData(final_ranked(i,ii),:) = [140 81 10]/256;
        elseif i == 8
            h.CData(final_ranked(i,ii),:) = [153 112 171]/256;
        elseif i == 9
            h.CData(final_ranked(i,ii),:) = [135 135 135]/256;
        else
            h.CData(final_ranked(i,ii),:) = [240 240 240]/256;
        end
    end
    box('off')
    grid('on')
    title(sprintf('Final feature ranking: %s data',strrep(mdls{ii,1},'_','-')),'FontSize',11)
    featurenames1 = strrep(featurenames','_','-');
    set(gca,'XTick',[1:1:n_features],'XTickLabel',featurenames1,'XTickLabelRotation',45,'fontsize',6);
    
    for i1 = 1:n_features
        text(final_ranked(i1,ii),Y(i1),num2str(i1),...
                   'HorizontalAlignment','center',...
                   'VerticalAlignment','bottom','FontSize',5)
    end
end