%% Get data
mydir = '/Users/psyc1586_admin/GVB_data/QC_Classifier_work/New-AdditionalFeatures';
fname = 'QC_classifier_additionalFeatures.xlsx';
sname = 'final';

datapath = fullfile(mydir,fname);
data     = readtable(datapath,'Sheet',sname);
datasets = {'bhc','opdc','whitehall1','whitehall2'};
data1    = data(ismember(data.dataset,datasets),:);

group_ids = data1.dataset;

%% Group scatter plots: selected measures only (MRIQC)
sel_mriqc_measures = {'cjv','cnr','efc','fber','fwhm_avg','inu_med','inu_range','qi_1','qi_2','snr_total','snrd_total','wm2max'};
iqm_mriqc = data1(:,ismember(data1.Properties.VariableNames,sel_mriqc_measures));
figure(1)
plot_data = table2array(iqm_mriqc);
labels    = iqm_mriqc.Properties.VariableNames;
varnames  = strrep(labels,'_','-');
gplotmatrix(plot_data,[],group_ids,{'red','green','cyan','yellow'},'.',6,[],'grpbar',varnames);
saveas(gcf,'mriqc_scattermatrix.fig')
close(gcf)

%% Group scatter plots: selected measures only (CAT12)
sel_cat_measures={'resolutionRMS_value','noiseNCR_value','biasICR_value','weightedIQR_value','surf_EulerNumber','surf_defectNumber','surf_DefectArea','surf_intensityRMSE','surf_positionRMSE'};
iqm_cat12 = data1(:,ismember(data1.Properties.VariableNames,sel_cat_measures));
figure(2)
plot_data = table2array(iqm_cat12);
labels    = iqm_cat12.Properties.VariableNames;
varnames = strrep(labels,'_','-');
gplotmatrix(plot_data,[],group_ids,{'red','green','cyan','yellow'},'.',6,[],'grpbar',varnames);
saveas(gcf,'cat12_scattermatrix.fig')
close(gcf)