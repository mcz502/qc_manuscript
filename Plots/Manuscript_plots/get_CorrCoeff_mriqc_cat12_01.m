mydir  = '/Users/psyc1586_admin/GVB_data/QC_Classifier_work/New-AdditionalFeatures/for_draft';
cd(mydir)
dataf  = 'main.xlsx';
sheetf = 'Sheet1';
file   = fullfile(mydir,dataf);
data   = readtable(file,'Sheet',sheetf);

data1 = data(:,13:end); % first feature CJV starts from here

% do correlation
[rcoeff,pval,rlower,rupper] = corrcoef(table2array(data1));

% copy rcoeff to excel and edit it; prepare for plotting correlation matrix