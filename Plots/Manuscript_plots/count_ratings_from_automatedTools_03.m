mydir  = '/Users/psyc1586_admin/GVB_data/QC_Classifier_work/New-AdditionalFeatures/for_draft';
cd(mydir)
dataf  = 'main.xlsx';
sheetf = 'Sheet1';
file   = fullfile(mydir,dataf);
data   = readtable(file,'Sheet',sheetf);

data1 = data(:,[1,2,8,9]); % just get the list of subject, dataset, CAT12 weighted IQR and MRIQC probability

%% CAT12 weighted IQR counts per dataset
rating_name  = unique(data1.weightedIQR_grade);
outs         = cell(length(rating_name),1);
for ii = 1:length(rating_name)
    temp                   = data1(ismember(data1.weightedIQR_grade,rating_name{ii,1}),:);
    [cnt_unique, unique_a] = hist(categorical(temp.dataset),unique(temp.dataset));
    vals                   = [unique_a', num2cell(cnt_unique')];
    add_column             = cellstr(repelem(rating_name{ii,1},size(vals,1),1));
    outs{ii,1}             = [add_column,vals];
end
cat12_iqr_counts = vertcat(outs{:});
% save(fullfile(mydir,'cat12_iqr_counts.mat'));

% plot?
temp_tab                          = cell2table(cat12_iqr_counts);
temp_tab.Properties.VariableNames = {'CAT12_weighted_IQR','Dataset','Counts'};
dataset_name                      = unique(temp_tab.Dataset);
for jj = 1:length(dataset_name)
    temp1 = temp_tab(ismember(temp_tab.Dataset,dataset_name{jj,1}),:);
    subplot(3,2,jj)
    bar(temp1.Counts, 'FaceColor',[254 230 206]./256)
    xticklabels(temp1.CAT12_weighted_IQR);
    xlabel('CAT12 weighted IQR','FontSize',8)
    ylabel('Total count','FontSize',8)
    title(upper(dataset_name{jj,1}))
    box('off')
    grid('minor')
    ax = gca;
    ax.FontSize = 6;
end
% now add total to last subplot
rating_name = unique(temp_tab.CAT12_weighted_IQR);
total_count = cell(length(rating_name),2);
for kk = 1:length(rating_name)
    total_count{kk,1} = rating_name{kk,1};
    total_count{kk,2} = sum(table2array(temp_tab(ismember(temp_tab.CAT12_weighted_IQR,rating_name{kk,1}),3)));
end 
subplot(3,2,length(dataset_name)+1)
bar(cell2mat(total_count(:,2)), 'FaceColor',[254 230 206]./256)
xticklabels(total_count(:,1));
xlabel('CAT12 weighted IQR','FontSize',8)
ylabel('Total count','FontSize',8)
title('All datasets')
box('off')
grid('minor')
ax = gca;
ax.FontSize = 6;
saveas(gcf,'CAT12_weighted_IQR_counts_03.fig')
print('CAT12_weighted_IQR_counts_03','-dpng','-r600');

%% MRIQC classifer prediction probability
dataset_name = unique(data1.dataset);
outs         = cell(length(dataset_name),8);
for mm = 1:length(dataset_name)
    temp3      = data1(ismember(data1.dataset,dataset_name{mm,1}),:);
    outs{mm,1} = dataset_name{mm,1};
    outs{mm,2} = sum(temp3.mriqc_prob>=0 & temp3.mriqc_prob<0.4);   % between 0.0 and 0.4
    outs{mm,3} = sum(temp3.mriqc_prob>=0.4 & temp3.mriqc_prob<0.5);   % between 0.4 and 0.5
    outs{mm,4} = sum(temp3.mriqc_prob>=0.5 & temp3.mriqc_prob<0.6); % between 0.5 and 0.6
    outs{mm,5} = sum(temp3.mriqc_prob>=0.6 & temp3.mriqc_prob<0.7); % between 0.6 and 0.6
    outs{mm,6} = sum(temp3.mriqc_prob>=0.7 & temp3.mriqc_prob<0.8); % between 0.7 and 0.7
    outs{mm,7} = sum(temp3.mriqc_prob>=0.8 & temp3.mriqc_prob<0.9); % between 0.8 and 0.8
    outs{mm,8} = sum(temp3.mriqc_prob>=0.9 & temp3.mriqc_prob<1);  % between 0.9 and 1.0
end
% plot?
outs_temp = cell2mat(outs(:,2:end)');
subplot(3,2,1)
bar(outs_temp(:,1), 'FaceColor',[254 230 206]./256)
xticklabels({'0.0<=prob<0.4','0.4<=prob<0.5','0.5<=prob<0.6','0.6<=prob<0.7','0.7<=prob<0.8','0.8<=prob<0.9','0.9<=prob<1'});
xlabel('MRIQC classifier probability','FontSize',8)
ylabel('Total count','FontSize',8)
title('ADNI')
box('off')
grid('minor')
ax = gca;
ax.FontSize = 6;

subplot(3,2,2)
bar(outs_temp(:,2), 'FaceColor',[254 230 206]./256)
xticklabels({'0.0<=prob<0.4','0.4<=prob<0.5','0.5<=prob<0.6','0.6<=prob<0.7','0.7<=prob<0.8','0.8<=prob<0.9','0.9<=prob<1'});
xlabel('MRIQC classifier probability','FontSize',8)
ylabel('Total count','FontSize',8)
title('BHC')
box('off')
grid('minor')
ax = gca;
ax.FontSize = 6;

subplot(3,2,3)
bar(outs_temp(:,3), 'FaceColor',[254 230 206]./256)
xticklabels({'0.0<=prob<0.4','0.4<=prob<0.5','0.5<=prob<0.6','0.6<=prob<0.7','0.7<=prob<0.8','0.8<=prob<0.9','0.9<=prob<1'});
xlabel('MRIQC classifier probability','FontSize',8)
ylabel('Total count','FontSize',8)
title('OPDC')
box('off')
grid('minor')
ax = gca;
ax.FontSize = 6;

subplot(3,2,4)
bar(outs_temp(:,4), 'FaceColor',[254 230 206]./256)
xticklabels({'0.0<=prob<0.4','0.4<=prob<0.5','0.5<=prob<0.6','0.6<=prob<0.7','0.7<=prob<0.8','0.8<=prob<0.9','0.9<=prob<1'});
xlabel('MRIQC classifier probability','FontSize',8)
ylabel('Total count','FontSize',8)
title('WHITEHALL1')
box('off')
grid('minor')
ax = gca;
ax.FontSize = 6;

subplot(3,2,5)
bar(outs_temp(:,5), 'FaceColor',[254 230 206]./256)
xticklabels({'0.0<=prob<0.4','0.4<=prob<0.5','0.5<=prob<0.6','0.6<=prob<0.7','0.7<=prob<0.8','0.8<=prob<0.9','0.9<=prob<1'});
xlabel('MRIQC classifier probability','FontSize',8)
ylabel('Total count','FontSize',8)
title('WHITEHALL2')
box('off')
grid('minor')
ax = gca;
ax.FontSize = 6;

total_count = sum(outs_temp,2);
subplot(3,2,6)
bar(total_count, 'FaceColor',[254 230 206]./256)
xticklabels({'0.0<=prob<0.4','0.4<=prob<0.5','0.5<=prob<0.6','0.6<=prob<0.7','0.7<=prob<0.8','0.8<=prob<0.9','0.9<=prob<1'});
xlabel('MRIQC classifier probability','FontSize',8)
ylabel('Total count','FontSize',8)
title('All datasets')
box('off')
grid('minor')
ax = gca;
ax.FontSize = 6;

saveas(gcf,'MRIQC_probability_counts_03.fig')
print('MRIQC_probability_counts_03','-dpng','-r600');




    



