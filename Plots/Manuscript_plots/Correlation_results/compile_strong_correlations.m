%% Copy pasted arrays and metric names from main.xlsx and saved to load_corr_data.mat

mydir = '/Users/psyc1586_admin/GVB_data/QC_Classifier_work/New-AdditionalFeatures/for_draft/correlation_results';
fname = 'load_corr_data.mat';

load(fullfile(mydir,fname))

% positive correlations
linearIndexes   = find(corr_val>0.5);  % Find elements with value more than 0.5
[rows, columns] = ind2sub(size(corr_val), linearIndexes);

pos_corr      = [mriqc(rows,1),cat12(1,columns)'];

for ii = 1:length(linearIndexes)
    pos_corr(ii,3) = num2cell(corr_val(rows(ii,1),columns(ii,1)));
    pos_corr(ii,4) = num2cell(p_val(rows(ii,1),columns(ii,1)));
    pos_corr(ii,5) = num2cell(ci_low(rows(ii,1),columns(ii,1)));
    pos_corr(ii,6) = num2cell(ci_high(rows(ii,1),columns(ii,1)));
end

% negative correlations
linearIndexes   = find(corr_val<(-0.5));  % Find elements with value more than 0.5
[rows, columns] = ind2sub(size(corr_val), linearIndexes);

neg_corr      = [mriqc(rows,1),cat12(1,columns)'];

for ii = 1:length(linearIndexes)
    neg_corr(ii,3) = num2cell(corr_val(rows(ii,1),columns(ii,1)));
    neg_corr(ii,4) = num2cell(p_val(rows(ii,1),columns(ii,1)));
    neg_corr(ii,5) = num2cell(ci_low(rows(ii,1),columns(ii,1)));
    neg_corr(ii,6) = num2cell(ci_high(rows(ii,1),columns(ii,1)));
end

% all correlations
all_corr     = [pos_corr;neg_corr];
all_corr_tab = cell2table(all_corr); 
all_corr_tab.Properties.VariableNames = {'MRIQC', 'CAT12', 'CorrCoeff' ,'p-value','Lower bound','Upper bound'};
writetable(all_corr_tab,fullfile(mydir,'compile_strong_correlations.csv'))