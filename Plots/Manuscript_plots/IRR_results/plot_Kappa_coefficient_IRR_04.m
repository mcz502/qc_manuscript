mydir = '/Users/psyc1586_admin/GVB_data/QC_Classifier_work/New-AdditionalFeatures/for_draft/IRR_results_04';

data = readtable(fullfile(mydir,'IRR_forPlots_04.xlsx'),'Sheet','for_plots');


dataset_name = unique(data.Dataset);
dataset_name{length(dataset_name)+1,1} = 'All datasets';
dataset_name(2,:) = [];

for cc = 1:length(dataset_name)
    temp = data(ismember(data.Dataset,dataset_name{cc,1}),:);
    subplot(3,2,cc)
    bar(temp.KappaCoefficient, 'FaceColor',[254 230 206]./256)
    xlabel('QC ratings','FontSize',8)
    ylabel('Kappa Coefficient','FontSize',8)
    title(upper(dataset_name{cc,1}))
    box('off')
    grid('minor')
    xticklabels(strrep(temp.Compare1,'_t','T'));
    ax = gca;
    ax.FontSize = 6; 
end
saveas(gcf,fullfile(mydir,'Kappa_coefficients_IRR_04.fig'))
print(fullfile(mydir,'Kappa_coefficients_IRR_04'),'-dpng','-r600');

