y = [127 201 127; 190 174 212;253 192 134]./256;
b = bar(a(:,1:3),'grouped'); % paste table from pptx to excel and store in variable
for k = 1:size(a,2)
    b(k).CData = k;
end


ylim([10 110])

ylabel('% Performance')
legend({'accuracy','sensitivity','specificity'},'Location','bestoutside');
xlabel('Model-Info')

xticklabels({'Mixed','Siemens3T-train','All3T-train',...
             'AllSiemens-train','PH1.5T-test','WH2-test',...
             'SI3T-ADNI-test','PH3T-test','GE1.5T-test','SI1.5T-test',...
             'WH1-test','BHC-test','SI2.9T-test','GE3T-test','OPDC-test'});
box('off')
grid('on')
saveas(gcf,'perf.fig')
print('perf','-dpng','-r900')