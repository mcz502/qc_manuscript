% Data

datafilepath = '/Users/psyc1586_admin/r_dpuk/QC_classifier_additionalMeasures';
datafile     = 'QC_classifier_additionalFeatures.xlsx';
data_tab     = readtable(fullfile(datafilepath,datafile),'Sheet','final');

try
    data_tab = removevars(data_tab, ["resolutionRMS_grade","biasICR_grade","noiseNCR_grade","weightedIQR_value","weightedIQR_rps","weightedIQR_grade","vol_TIV"]);
catch
    warning('Probably the extra columns do not exist in data');
end

sitenames = unique(data_tab.Scanner);
ratingCol = 'NewManual';

gtrain = cell(length(sitenames),1);
gtest  = cell(length(sitenames),1);

for site = 1:length(sitenames)
    temp            = data_tab(ismember(data_tab.Scanner,sitenames{site,1}),:);
    rng(1000)
    gpartition      = cvpartition(temp.(ratingCol),'HoldOut',0.2);
    gtrain{site,1}  = temp(gpartition.training,:);
    gtest{site,1}   = temp(gpartition.test,:);
end

all_train = vertcat(gtrain{:});
all_test  = vertcat(gtest{:});

%% Get the counts

data_tab = all_test;
sites   = unique(data_tab.Scanner);
site_qc = cell(length(sites),4);
for site = 1:length(sites)
    temp_data = data_tab(ismember(data_tab.Scanner,sites{site,1}),:);
    temp_qc   = temp_data.NewManual;
    site_qc{site,1} = sites{site,1};
    site_qc{site,2} = sum(temp_qc);
    site_qc{site,3} = length(temp_qc) - sum(temp_qc);
    site_qc{site,4} = length(temp_qc);
end
site_qc_tab_test = cell2table(site_qc);
site_qc_tab_test.Properties.VariableNames = {'site','reject','accept','total'};

data_tab = all_train;
sites   = unique(data_tab.Scanner);
site_qc = cell(length(sites),4);
for site = 1:length(sites)
    temp_data = data_tab(ismember(data_tab.Scanner,sites{site,1}),:);
    temp_qc   = temp_data.NewManual;
    site_qc{site,1} = sites{site,1};
    site_qc{site,2} = sum(temp_qc);
    site_qc{site,3} = length(temp_qc) - sum(temp_qc);
    site_qc{site,4} = length(temp_qc);
end
site_qc_tab_train = cell2table(site_qc);
site_qc_tab_train.Properties.VariableNames = {'site','reject','accept','total'};

%% Save variables
out = 'Mixed_data';
if ~exist(fullfile(datafilepath,out),'dir')
    mkdir(fullfile(datafilepath,out))
    save(fullfile(datafilepath,out,'train_test_data.mat'),'all_train','all_test','site_qc_tab_train','site_qc_tab_test');
elseif exist(fullfile(datafilepath,out,'train_test_data.mat'),'file')
    error('This data have been processed before! To run again delete the train_test_data.mat file from outs folder!');
else
    save(fullfile(datafilepath,out,'train_test_data.mat'),'all_train','all_test','site_qc_tab_train','site_qc_tab_test');
end