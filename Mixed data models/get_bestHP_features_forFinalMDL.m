%% Load data
data_dir = '/Users/psyc1586_admin/r_dpuk/QC_classifier_additionalMeasures/Mixed_data';
load(fullfile(data_dir,'train_test_data.mat'));
mydir = fullfile(data_dir,'outs_svm_rf_corrected');
files = dir(fullfile(mydir,'run*.mat'));

%% Get best hp: SVM

data_svm  = cell(0);
data_rf   = cell(0);

for file = 1:length(files)
    fname = fullfile(files(file).folder,files(file).name);
    load(fname, 'hp_save_outer_svm' , 'hp_save_outer_rf');    
    data_svm = [data_svm;hp_save_outer_svm(:,1)];
    data_rf =  [data_rf;hp_save_outer_rf(:,1)];
end
% Get feature-wise HP
data_temp   = cell2mat(data_svm');
hp1_temp    = data_temp(:,1:2:end);
hp2_temp    = data_temp(:,2:2:end);
hp1_mode    = mode(hp1_temp,2);
hp2_mode    = mode(hp2_temp,2);
best_hp_svm = [hp1_mode,hp2_mode];
clear hp1_temp hp2_temp hp1_mode hp2_mode data_temp

data_temp   = cell2mat(data_rf');
hp1_temp    = data_temp(:,1:2:end);
hp2_temp    = data_temp(:,2:2:end);
hp1_mode    = mode(hp1_temp,2);
hp2_mode    = mode(hp2_temp,2);
best_hp_rf = [hp1_mode,hp2_mode];
clear hp1_temp hp2_temp hp1_mode hp2_mode 

save(fullfile(mydir,'best_hp_per_feature_svm.mat'),'best_hp_svm');
save(fullfile(mydir,'best_hp_per_feature_rf.mat'),'best_hp_rf');

%% Rank features
RankedFeatures = cell(1,length(files));

for ii = 1:length(files)
    fname                 = fullfile(files(ii).folder,files(ii).name);
    load(fname,'Outer_selected_features')
    [aggR, ~, rowNames]   = aggregateRanks(Outer_selected_features);
    agg_fR                = [aggR,rowNames];
    rankedFeatures        = sortrows(agg_fR,1);
    RankedFeatures{:,ii}  = rankedFeatures(:,2);
end

[aggR, ~, rowNames]   = aggregateRanks(RankedFeatures);
agg_fR                = [aggR,rowNames];
rankedFeatures        = sortrows(agg_fR,1);
final_ranked          = rankedFeatures(:,2);
save(fullfile(mydir,'final_ranked.mat'),'final_ranked');