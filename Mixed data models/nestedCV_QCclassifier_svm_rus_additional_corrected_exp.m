function nestedCV_QCclassifier_svm_rus_additional_corrected_exp(hyperparafilepath_svm,hyperparafilepath_rf, datamat,outfolds,infolds,featurestart,featureset,ratingsCol,siteCol,runClassifier,outpath,run_no)
    

    load(hyperparafilepath_svm,'grSearch_svm');  
    load(hyperparafilepath_rf,'grSearch_rf');   
%     load(hyperparafilepath_rus,'grSearch_rus');    

    load(datamat,'train_site')
    
    gtrain = train_site;

    % Cross validation
    Ofolds      = outfolds;
    Ifolds      = infolds;
    startCol    = featurestart;
    rater_n     = ratingsCol;
    site_n      = siteCol;
    
    % ClassNames
    
    classNames  = unique(gtrain.(rater_n));
    classNames  = classNames';     % Set the classes here 
    
    NumFeatures = length(featureset);


    % Initialize outputs
    if runClassifier == 1
        Oacc_svm                = zeros(Ofolds,NumFeatures);
        Osens_svm               = zeros(Ofolds,NumFeatures);
        Ospec_svm               = zeros(Ofolds,NumFeatures);
        Oblacc_svm              = zeros(Ofolds,NumFeatures);
        Opres_svm               = zeros(Ofolds,NumFeatures);
        Of1score_svm            = zeros(Ofolds,NumFeatures);
        hp_save_outer_svm       = cell(Ofolds,NumFeatures);

        Oacc_rf                = zeros(Ofolds,NumFeatures);
        Osens_rf               = zeros(Ofolds,NumFeatures);
        Ospec_rf               = zeros(Ofolds,NumFeatures);
        Oblacc_rf              = zeros(Ofolds,NumFeatures);
        Opres_rf               = zeros(Ofolds,NumFeatures);
        Of1score_rf            = zeros(Ofolds,NumFeatures);
        hp_save_outer_rf       = cell(Ofolds,NumFeatures);

%         Oacc_rus                = zeros(Ofolds,NumFeatures);
%         Osens_rus               = zeros(Ofolds,NumFeatures);
%         Ospec_rus               = zeros(Ofolds,NumFeatures);
%         Oblacc_rus              = zeros(Ofolds,NumFeatures);
%         Opres_rus               = zeros(Ofolds,NumFeatures);
%         Of1score_rus            = zeros(Ofolds,NumFeatures);
%         hp_save_outer_rus       = cell(Ofolds,NumFeatures);
    end
    Outer_selected_features = cell(1,Ofolds);
    Inner_selected_features = cell(Ifolds,Ofolds);
    
    % Outer CV 
    
    Opartition = cvpartition(gtrain.(rater_n),'KFold',Ofolds);
    
    for i = 1:Ofolds % for each outer fold do following:
    
        % Get the training data and targets in outer fold
        Otrain_idx  = training(Opartition,i);
        Otrain      = gtrain(Otrain_idx==1,:); 
    
        % Get the testing data and targets in outer fold
        Otest_idx   = test(Opartition,i);
        Otest       = gtrain(Otest_idx==1,:);
    
        % Do inner CV partition
        Ipartition  = cvpartition(Otrain.(rater_n),'KFold',Ifolds);
        
        if runClassifier == 1
            Iacc_svm_perfold        = cell(1,Ifolds);
            Isens_svm_perfold       = cell(1,Ifolds);
            Ispec_svm_perfold       = cell(1,Ifolds);
            Iblacc_svm_perfold      = cell(1,Ifolds);
            Ipres_svm_perfold       = cell(1,Ifolds);
            If1score_svm_perfold    = cell(1,Ifolds);

            Iacc_rf_perfold        = cell(1,Ifolds);
            Isens_rf_perfold       = cell(1,Ifolds);
            Ispec_rf_perfold       = cell(1,Ifolds);
            Iblacc_rf_perfold      = cell(1,Ifolds);
            Ipres_rf_perfold       = cell(1,Ifolds);
            If1score_rf_perfold    = cell(1,Ifolds);

%             Iacc_rus_perfold        = cell(1,Ifolds);
%             Isens_rus_perfold       = cell(1,Ifolds);
%             Ispec_rus_perfold       = cell(1,Ifolds);
%             Iblacc_rus_perfold      = cell(1,Ifolds);
%             Ipres_rus_perfold       = cell(1,Ifolds);
%             If1score_rus_perfold    = cell(1,Ifolds);
        end
    
        for i1 = 1:Ifolds % For each nested fold do following:
                
                % Get the train data and targets
                Idata_idx      = training(Ipartition,i1);
                Itrain         = Otrain(Idata_idx==1,:);
                
                % Get the test data and targets
                Itest_data_idx = test(Ipartition,i1);
                Itest          = Otrain(Itest_data_idx==1,:);
                
                % Get column ids for feature start, rating and site
                Sidx           = find(string(Itrain.Properties.VariableNames) == startCol);
                Ridx           = find(string(Itrain.Properties.VariableNames) == rater_n);
                St_idx         = find(string(Itrain.Properties.VariableNames) == site_n);
                
                % scale feature: site-specific
                sitenames      = table2cell(unique(Itrain(:,St_idx)));
                temp_train     = cell(length(sitenames),1);
                temp_test      = cell(length(sitenames),1);            
    
                for St = 1:length(sitenames)
                    temp_train{St,1}  = Itrain(ismember(table2cell(Itrain(:,St_idx)),sitenames{St,1}),:); % train data
                    temp_test{St,1}   = Itest(ismember(table2cell(Itest(:,St_idx)),sitenames{St,1}),:); % train data
                end
               
                % Store the scaled features (in table) in cell arrays
                tmp_train        = cell(length(sitenames),1);
                tmp_test         = cell(length(sitenames),1);
                tmp_train_target = cell(length(sitenames),1);
                tmp_test_target  = cell(length(sitenames),1);
                
                try
                    for tt = 1:length(sitenames)
                        tmp_train1                       = table2array(temp_train{tt,1}(:,Sidx:end));
                        tmp_test1                        = table2array(temp_test{tt,1}(:,Sidx:end));
                        [tmp_train{tt,1},tmp_test{tt,1}] = scale_train_test(tmp_train1,tmp_test1);
                        tmp_train_target{tt,1}           = table2array(temp_train{tt,1}(:,Ridx));
                        tmp_test_target{tt,1}            = table2array(temp_test{tt,1}(:,Ridx));
                    end
                end
    
                % tmp_train and tmp_test contain the site-specific scaled train
                % and test data; on test data the mean and standard deviation
                % of train data are applied (not separately done)
    
                train_data          = cell2mat({cat(1,tmp_train{:})});
                test_data           = cell2mat({cat(1,tmp_test{:})});
                train_target        = cell2mat({cat(1,tmp_train_target{:})});
                test_target         = cell2mat({cat(1,tmp_test_target{:})});
    
                train_data_target   = [train_target, train_data];
                test_data_target    = [test_target, test_data];
                shuffled_train      =  train_data_target(randperm(size(train_data_target,1)),:);
    
                sTrain_data         = shuffled_train(:,2:end);
                sTrain_target       = shuffled_train(:,1);
                Test_data           = test_data_target(:,2:end);
                Test_target         = test_data_target(:,1);
    
                RAR_RankedFeatures  = RobustAggRanking(sTrain_data,sTrain_target);
                ranked_features     = RAR_RankedFeatures(:,2);
                Inner_selected_features{i1,i} = ranked_features;
                
                if runClassifier == 1
                    % initialize the output variables
                    Iacc_svm            = zeros(length(grSearch_svm),NumFeatures);
                    Isens_svm           = zeros(length(grSearch_svm),NumFeatures); 
                    Iblacc_svm          = zeros(length(grSearch_svm),NumFeatures);
                    Ipres_svm           = zeros(length(grSearch_svm),NumFeatures);
                    If1score_svm        = zeros(length(grSearch_svm),NumFeatures);
                    Ispec_svm           = zeros(length(grSearch_svm),NumFeatures);

                    Iacc_rf            = zeros(length(grSearch_rf),NumFeatures);
                    Isens_rf           = zeros(length(grSearch_rf),NumFeatures); 
                    Iblacc_rf          = zeros(length(grSearch_rf),NumFeatures);
                    Ipres_rf           = zeros(length(grSearch_rf),NumFeatures);
                    If1score_rf        = zeros(length(grSearch_rf),NumFeatures);
                    Ispec_rf           = zeros(length(grSearch_rf),NumFeatures);

%                     Iacc_rus            = zeros(length(grSearch_rus),NumFeatures);
%                     Isens_rus           = zeros(length(grSearch_rus),NumFeatures); 
%                     Iblacc_rus          = zeros(length(grSearch_rus),NumFeatures);
%                     Ipres_rus           = zeros(length(grSearch_rus),NumFeatures);
%                     If1score_rus        = zeros(length(grSearch_rus),NumFeatures);
%                     Ispec_rus           = zeros(length(grSearch_rus),NumFeatures);
        
                    for i3 = 1:NumFeatures
                        features = sTrain_data(:,ranked_features(1:featureset(i3,1),1));
        
                        for alg1 = 1:length(grSearch_svm)
                            
                            mdl_svm         = fitcsvm(features,sTrain_target,'BoxConstraint',grSearch_svm(alg1,1),'KernelFunction','linear','KernelScale',grSearch_svm(alg1,2));
                            
                            % Predict the performance of the model on nested fold test data
                            Iprediction_svm = predict(mdl_svm,Test_data(:,ranked_features(1:featureset(i3,1),1)));
                            
                            % Save the model, selected features, and accuracy for further
                            % inspection
                            [Iacc_svm(alg1,i3),Isens_svm(alg1,i3),...
                             Ispec_svm(alg1,i3),Iblacc_svm(alg1,i3),...
                             Ipres_svm(alg1,i3),If1score_svm(alg1,i3)]...
                                = calc_confusionmat_measures(Test_target,Iprediction_svm,classNames);
                        end
        
                        for alg1 = 1:length(grSearch_rf)
                            t              = templateTree('MaxNumSplits',grSearch_rf(alg1,1));
                            mdl_rf         = fitcensemble(features,sTrain_target,'Method','Bag','Learners',t,'NumLearningCycles',grSearch_rf(alg1,2));
                            
                            % Predict the performance of the model on nested fold test data
                            Iprediction_rf = predict(mdl_rf,Test_data(:,ranked_features(1:featureset(i3,1),1)));
                            
                            % Save the model, selected features, and accuracy for further
                            % inspection
                            [Iacc_rf(alg1,i3),Isens_rf(alg1,i3),...
                             Ispec_rf(alg1,i3),Iblacc_rf(alg1,i3),...
                             Ipres_rf(alg1,i3),If1score_rf(alg1,i3)]...
                                = calc_confusionmat_measures(Test_target,Iprediction_rf,classNames);
                        end

%                         for alg1 = 1:length(grSearch_rus)
%                             t              = templateTree('MaxNumSplits',grSearch_rus(alg1,1));
%                             mdl_rus        = fitcensemble(features,sTrain_target,'Method','RUSBoost','Learners',t,'NumLearningCycles',grSearch_rus(alg1,2),'LearnRate',grSearch_rus(alg1,3));
%                             
%                             % Predict the performance of the model on nested fold test data
%                             Iprediction_rus = predict(mdl_rus,Test_data(:,ranked_features(1:featureset(i3,1),1)));
%                             
%                             % Save the model, selected features, and accuracy for further
%                             % inspection
%                             [Iacc_rus(alg1,i3),Isens_rus(alg1,i3),...
%                              Ispec_rus(alg1,i3),Iblacc_rus(alg1,i3),...
%                              Ipres_rus(alg1,i3),If1score_rus(alg1,i3)]...
%                                 = calc_confusionmat_measures(Test_target,Iprediction_rus,classNames);
%                         end

                        Iacc_svm_perfold{1,i1}     = Iacc_svm;
                        Isens_svm_perfold{1,i1}    = Isens_svm;
                        Ispec_svm_perfold{1,i1}    = Ispec_svm;
                        Ipres_svm_perfold{1,i1}    = Ipres_svm;
                        Iblacc_svm_perfold{1,i1}   = Iblacc_svm;
                        If1score_svm_perfold{1,i1} = If1score_svm;  

                        Iacc_rf_perfold{1,i1}     = Iacc_rf;
                        Isens_rf_perfold{1,i1}    = Isens_rf;
                        Ispec_rf_perfold{1,i1}    = Ispec_rf;
                        Ipres_rf_perfold{1,i1}    = Ipres_rf;
                        Iblacc_rf_perfold{1,i1}   = Iblacc_rf;
                        If1score_rf_perfold{1,i1} = If1score_rf;  

%                         Iacc_rus_perfold{1,i1}     = Iacc_rus;
%                         Isens_rus_perfold{1,i1}    = Isens_rus;
%                         Ispec_rus_perfold{1,i1}    = Ispec_rus;
%                         Ipres_rus_perfold{1,i1}    = Ipres_rus;
%                         Iblacc_rus_perfold{1,i1}   = Iblacc_rus;
%                         If1score_rus_perfold{1,i1} = If1score_rus;  

                        fprintf('\n Run %d : Running --- %d feature no; %d inner cv; %d outer cv \n',run_no,i3,i1,i);
                    end
                    fprintf('\n Run %d : All features done --- %d inner cv; %d outer cv \n',run_no,i1,i)
                end
        end

        if runClassifier == 1
            % Collate everything from inner CVs and all the features: Accuracy
            fset_svm = cell(Ifolds,NumFeatures);
            temp_svm = zeros(length(grSearch_svm),Ifolds);
            fset_rf = cell(Ifolds,NumFeatures);
            temp_rf = zeros(length(grSearch_rf),Ifolds);
%             fset_rus = cell(Ifolds,NumFeatures);
%             temp_rus = zeros(length(grSearch_rus),Ifolds);
        
            for z1 = 1:NumFeatures
                for y1 = 1:Ifolds
                    fset_svm{y1,z1}   = Iacc_svm_perfold{1,y1}(:,z1);
                    fset_rf{y1,z1}    = Iacc_rf_perfold{1,y1}(:,z1);
%                     fset_rus{y1,z1}   = Iacc_rus_perfold{1,y1}(:,z1);
               end     
            end
            best_para_svm1 = zeros(NumFeatures,size(grSearch_svm,2));
            best_para_rf1  = zeros(NumFeatures,size(grSearch_rf,2));

            for z1 = 1:NumFeatures
                for y1 = 1:Ifolds
                   temp_rf(:,y1)  = fset_rf{y1,z1};
                   temp_rf(:,y1)  = fset_rf{y1,z1};
%                    temp_rus(:,y1) = fset_rus{y1,z1};
                end
               % best parameters SVM
               AvgAcc_svm       = mean(temp_svm,2);
               BestInnerAcc_svm = max(AvgAcc_svm);
               best_para_svm    = grSearch_svm(AvgAcc_svm==BestInnerAcc_svm,:);

               AvgAcc_rf       = mean(temp_rf,2);
               BestInnerAcc_rf = max(AvgAcc_rf);
               best_para_rf    = grSearch_rf(AvgAcc_rf==BestInnerAcc_rf,:);

%                AvgAcc_rus       = mean(temp_rus,2);
%                BestInnerAcc_rus = max(AvgAcc_rus);
%                best_para_rus    = grSearch_rus(AvgAcc_rus==BestInnerAcc_rus,:);

               % Apply some heuristics if performance measure matches for more than two hyperparameters
               if size(best_para_svm,1)>= size(grSearch_svm,2)
                   best_para_svm1(z1,:) = best_para_svm(randperm(length(best_para_svm),1),:);
               else
                   best_para_svm1(z1,:) = best_para_svm;
               end
               if size(best_para_rf,1)>= size(grSearch_rf,2)
                   best_para_rf1(z1,:) = best_para_rf(randperm(length(best_para_rf),1),:);
               else
                   best_para_rf1(z1,:) = best_para_rf;
               end

%                if size(best_para_rus,1)>= size(grSearch_rus,2)
%                    best_para_rus1 = best_para_rus(randperm(length(best_para_rus),1),:);
%                 else
%                    best_para_rus1 = best_para_rus;
%                end

            end 
        end
        
        % Get column ids for feature start, rating and site
        Sidx           = find(string(Otrain.Properties.VariableNames) == startCol);
        Ridx           = find(string(Otrain.Properties.VariableNames) == rater_n);
        St_idx         = find(string(Otrain.Properties.VariableNames) == site_n);
        
        % scale feature: site-specific
        sitenames      = table2cell(unique(Otrain(:,St_idx)));
        temp_Otrain    = cell(length(sitenames),1);
        temp_Otest     = cell(length(sitenames),1);            
    
        for St = 1:length(sitenames)
            temp_Otrain{St,1}  = Otrain(ismember(table2cell(Otrain(:,St_idx)),sitenames{St,1}),:); % train data
            temp_Otest{St,1}   = Otest(ismember(table2cell(Otest(:,St_idx)),sitenames{St,1}),:); % train data
        end
       
        % Store the scaled features (in table) in cell arrays
        tmp_Otrain        = cell(length(sitenames),1);
        tmp_Otest         = cell(length(sitenames),1);
        tmp_Otrain_target = cell(length(sitenames),1);
        tmp_Otest_target  = cell(length(sitenames),1);
        
        try
            for tt = 1:length(sitenames)
                tmp_Otrain1                        = table2array(temp_Otrain{tt,1}(:,Sidx:end));
                tmp_Otest1                         = table2array(temp_Otest{tt,1}(:,Sidx:end));
                [tmp_Otrain{tt,1},tmp_Otest{tt,1}] = scale_train_test(tmp_Otrain1,tmp_Otest1);
                tmp_Otrain_target{tt,1}            = table2array(temp_Otrain{tt,1}(:,Ridx));
                tmp_Otest_target{tt,1}             = table2array(temp_Otest{tt,1}(:,Ridx));
            end
        end
    
        % tmp_train and tmp_test contain the site-specific scaled train
        % and test data; on test data the mean and standard deviation
        % of train data are applied (not separately done)
    
        Otrain_data          = cell2mat({cat(1,tmp_Otrain{:})});
        Otest_data           = cell2mat({cat(1,tmp_Otest{:})});
        Otrain_target        = cell2mat({cat(1,tmp_Otrain_target{:})});
        Otest_target         = cell2mat({cat(1,tmp_Otest_target{:})});
    
        Otrain_data_target   = [Otrain_target, Otrain_data];
        Otest_data_target    = [Otest_target, Otest_data];
        shuffled_Otrain      =  Otrain_data_target(randperm(size(Otrain_data_target,1)),:);
    
        sOTrain_data         = shuffled_Otrain(:,2:end);
        sOTrain_target       = shuffled_Otrain(:,1);
        OTest_data           = Otest_data_target(:,2:end);
        OTest_target         = Otest_data_target(:,1);
    
        RAR_RankedFeatures  = RobustAggRanking(sOTrain_data,sOTrain_target);
        ranked_features     = RAR_RankedFeatures(:,2);
        
        if NumFeatures == 0
            NumFeatures         = size(sOTrain_data,2);
        end

        Outer_selected_features{1,i} = ranked_features;
        
        if runClassifier == 1
            for i4 = 1:NumFeatures
                features_outer  = sOTrain_data(:,ranked_features(1:featureset(i4,1),1));
                % Train and test the SVM model with best hyperparamters 
                mdl_svm              = fitcsvm(features_outer,sOTrain_target,'BoxConstraint',best_para_svm1(i4,1),'KernelFunction','linear','KernelScale',best_para_svm1(i4,2));
                hp_save_outer_svm{i,i4}  = best_para_svm1;
                % Predict the performance of the model on nested fold test data
                Oprediction_svm = predict(mdl_svm,OTest_data(:,ranked_features(1:featureset(i4,1),1)));
                % Save the model, selected features, and accuracy for further
                % inspection            
                [Oacc_svm(i,i4),Osens_svm(i,i4),Ospec_svm(i,i4),...
                 Oblacc_svm(i,i4),Opres_svm(i,i4),Of1score_svm(i,i4)]...
                    = calc_confusionmat_measures(OTest_target,Oprediction_svm,classNames);
                
                t              = templateTree('MaxNumSplits',best_para_rf1(i4,1));
                mdl_rf        = fitcensemble(features_outer,sOTrain_target,'Method','Bag','Learners',t,'NumLearningCycles',best_para_rf1(i4,2));
                hp_save_outer_rf{i,i4}  = best_para_rf1;
                % Predict the performance of the model on nested fold test data
                Oprediction_rf          = predict(mdl_rf,OTest_data(:,ranked_features(1:featureset(i4,1),1)));
                % Save the model, selected features, and accuracy for further
                % inspection            
                [Oacc_rf(i,i4),Osens_rf(i,i4),Ospec_rf(i,i4),...
                 Oblacc_rf(i,i4),Opres_rf(i,i4),Of1score_rf(i,i4)]...
                    = calc_confusionmat_measures(OTest_target,Oprediction_rf,classNames);

%                 t              = templateTree('MaxNumSplits',best_para_rus1(1,1));
%                 mdl_rus        = fitcensemble(features_outer,sOTrain_target,'Method','RUSBoost','Learners',t,'NumLearningCycles',best_para_rus1(1,2),'LearnRate',best_para_rus1(1,3));
%                 hp_save_outer_rus{i,i4}  = best_para_rus1;
%                 % Predict the performance of the model on nested fold test data
%                 Oprediction_rus          = predict(mdl_rus,OTest_data(:,ranked_features(1:featureset(i4,1),1)));
%                 % Save the model, selected features, and accuracy for further
%                 % inspection            
%                 [Oacc_rus(i,i4),Osens_rus(i,i4),Ospec_rus(i,i4),...
%                  Oblacc_rus(i,i4),Opres_rus(i,i4),Of1score_rus(i,i4)]...
%                     = calc_confusionmat_measures(OTest_target,Oprediction_rus,classNames);

                 fprintf('\n Run %d :  Running --- %d feature no; %d outer cv \n',run_no,i4,i);
             end
             fprintf('\n Run %d : Completed --- %d outer cv \n',run_no,i);
        end
    end
    
    % Collate final scores
    if runClassifier == 1
        CV_mean_acc_svm     = mean(Oacc_svm);
        CV_std_acc_svm      = std(Oacc_svm);
        CV_mean_sens_svm    = mean(Osens_svm);
        CV_std_sens_svm     = std(Osens_svm);
        CV_mean_spec_svm    = mean(Ospec_svm);
        CV_std_spec_svm     = std(Ospec_svm);
        CV_mean_blacc_svm   = mean(Oblacc_svm);
        CV_std_blacc_svm    = std(Oblacc_svm);
        CV_mean_pres_svm    = mean(Opres_svm);
        CV_std_pres_svm     = std(Opres_svm);
        CV_mean_f1score_svm = mean(Of1score_svm);
        CV_std_f1score_svm  = std(Of1score_svm);
        
        all_performance_svm = [CV_mean_acc_svm;CV_mean_sens_svm;...
                           CV_mean_spec_svm;CV_mean_blacc_svm;...
                           CV_mean_pres_svm;CV_mean_f1score_svm];
        all_std_svm         = [CV_std_acc_svm;CV_std_sens_svm;...
                           CV_std_spec_svm;CV_std_blacc_svm;...
                           CV_std_pres_svm;CV_std_f1score_svm];

        CV_mean_acc_rf     = mean(Oacc_rf);
        CV_std_acc_rf      = std(Oacc_rf);
        CV_mean_sens_rf    = mean(Osens_rf);
        CV_std_sens_rf     = std(Osens_rf);
        CV_mean_spec_rf    = mean(Ospec_rf);
        CV_std_spec_rf     = std(Ospec_rf);
        CV_mean_blacc_rf   = mean(Oblacc_rf);
        CV_std_blacc_rf    = std(Oblacc_rf);
        CV_mean_pres_rf    = mean(Opres_rf);
        CV_std_pres_rf     = std(Opres_rf);
        CV_mean_f1score_rf = mean(Of1score_rf);
        CV_std_f1score_rf  = std(Of1score_rf);
        
        all_performance_rf = [CV_mean_acc_rf;CV_mean_sens_rf;...
                           CV_mean_spec_rf;CV_mean_blacc_rf;...
                           CV_mean_pres_rf;CV_mean_f1score_rf];
        all_std_rf         = [CV_std_acc_rf;CV_std_sens_rf;...
                           CV_std_spec_rf;CV_std_blacc_rf;...
                           CV_std_pres_rf;CV_std_f1score_rf];

%         CV_mean_acc_rus     = mean(Oacc_rus);
%         CV_std_acc_rus      = std(Oacc_rus);
%         CV_mean_sens_rus    = mean(Osens_rus);
%         CV_std_sens_rus     = std(Osens_rus);
%         CV_mean_spec_rus    = mean(Ospec_rus);
%         CV_std_spec_rus     = std(Ospec_rus);
%         CV_mean_blacc_rus   = mean(Oblacc_rus);
%         CV_std_blacc_rus    = std(Oblacc_rus);
%         CV_mean_pres_rus    = mean(Opres_rus);
%         CV_std_pres_rus     = std(Opres_rus);
%         CV_mean_f1score_rus = mean(Of1score_rus);
%         CV_std_f1score_rus  = std(Of1score_rus);
%         
%         all_performance_rus = [CV_mean_acc_rus;CV_mean_sens_rus;...
%                            CV_mean_spec_rus;CV_mean_blacc_rus;...
%                            CV_mean_pres_rus;CV_mean_f1score_rus];
%         all_std_rus         = [CV_std_acc_rus;CV_std_sens_rus;...
%                            CV_std_spec_rus;CV_std_blacc_rus;...
%                            CV_std_pres_rus;CV_std_f1score_rus];


        save(fullfile(outpath,sprintf('run_%d.mat',run_no)))
        fprintf('\n --- Finished (%d) : Saved the results --- \n', run_no);
    else
        save(fullfile(outpath,sprintf('features_run_%d.mat',run_no)),'Outer_selected_features','Inner_selected_features')
        fprintf('\n --- Feature ranking finished (%d) : Saved the results --- \n',run_no);
    end
end

