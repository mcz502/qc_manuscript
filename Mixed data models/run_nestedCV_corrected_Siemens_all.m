hyperparafilepath_svm = '/vols/Data/pipelines-DPUK/QC_classifier_additionalMeasures/Mixed_data/grSearch_svm.mat';
hyperparafilepath_rf  = '/vols/Data/pipelines-DPUK/QC_classifier_additionalMeasures/Mixed_data/grSearch_rf.mat';

datamat           = '/vols/Data/pipelines-DPUK/QC_classifier_additionalMeasures/Mixed_data/outs_svm_rf_Siemens_all_train/train_test_data.mat';
outfolds          = 5;
infolds           = 3;
featurestart      = 'cjv';
ratingsCol        = 'NewManual';
siteCol           = 'Scanner';
runClassifier     = 1;
featureset        = [10,20,30,40,50,60,70,80,90,100,104]';

outpath           = '/vols/Data/pipelines-DPUK/QC_classifier_additionalMeasures/Mixed_data/outs_svm_rf_Siemens_all_train';
if ~exist(outpath,'dir')
    mkdir(outpath)
end

n = 50;
parfor mul_runs = 1:n
    nestedCV_QCclassifier_svm_rus_additional_corrected_exp(hyperparafilepath_svm,hyperparafilepath_rf, datamat,outfolds,infolds,featurestart,featureset,ratingsCol,siteCol,runClassifier,outpath,mul_runs)
end