%% Load data
data_dir = '/Users/psyc1586_admin/r_dpuk/QC_classifier_additionalMeasures/Mixed_data';

run_dir  = 'outs_svm_rf_FieldStrength';
load(fullfile(data_dir,run_dir,'train_test_data.mat'));

load(fullfile(data_dir,run_dir,'best_hp_per_feature_rf.mat'));
load(fullfile(data_dir,run_dir,'best_hp_per_feature_svm.mat'));
load(fullfile(data_dir,run_dir,'final_ranked.mat'));

featureset        = [10,20,30,40,50,60,70,80,90,100,104]';

%%

% Train classifier with best HP
featurestart   = 'cjv';
ratingsCol     = 'NewManual';
siteCol        = 'Scanner';

% initialize train and test data
gtrain = train_site;
gtest  = test_site;

classNames  = unique(gtrain.(ratingsCol));
classNames  = classNames';     % Set the classes here 
Sidx        = find(string(gtrain.Properties.VariableNames) == featurestart);
NumFeatures = length(featureset);



% Get column ids for feature start, rating and site
Sidx           = find(string(gtrain.Properties.VariableNames) == featurestart);
Ridx           = find(string(gtrain.Properties.VariableNames) == ratingsCol);

train_data   = table2array(gtrain(:,Sidx:end));
test_data    = table2array(gtest(:,Sidx:end));
train_target = table2array(gtrain(:,Ridx));
test_target  = table2array(gtest(:,Ridx));

[tmp_train,tmp_test] = scale_train_test(train_data,test_data);

train_data_target   = [train_target, tmp_train];
test_data_target    = [test_target, tmp_test];
rng(1)
shuffled_train      =  train_data_target(randperm(size(train_data_target,1)),:);

sTrain_data         = shuffled_train(:,2:end);
sTrain_target       = shuffled_train(:,1);
Test_data           = test_data_target(:,2:end);
Test_target         = test_data_target(:,1);

ranked_features     = final_ranked(:,1);

perf_svm = zeros(6,NumFeatures);
perf_rf  = zeros(6,NumFeatures);
perf_rus = zeros(6,NumFeatures);

Iprediction_svm = zeros(length(Test_target),NumFeatures);
Iprediction_rf  = zeros(length(Test_target),NumFeatures);
Iprediction_rus = zeros(length(Test_target),NumFeatures);

for i3 = 1:NumFeatures
    features = sTrain_data(:,ranked_features(1:featureset(i3,1),1));
    
    mdl_svm  = fitcsvm(features,sTrain_target,'BoxConstraint',best_hp_svm(i3,1),'KernelFunction','linear','KernelScale',1);
    t_rf     = templateTree('MaxNumSplits',best_hp_rf(i3,1));
    mdl_rf   = fitcensemble(features,sTrain_target,'Method','Bag','Learners',t_rf,'NumLearningCycles',best_hp_rf(i3,2));
                           
    Iprediction_svm(:,i3) = predict(mdl_svm,Test_data(:,ranked_features(1:featureset(i3,1),1)));
    Iprediction_rf(:,i3)  = predict(mdl_rf,Test_data(:,ranked_features(1:featureset(i3,1),1)));
    
    [perf_svm(1,i3),perf_svm(2,i3),...
     perf_svm(3,i3),perf_svm(4,i3),...
     perf_svm(5,i3),perf_svm(6,i3)]...
        = calc_confusionmat_measures(Test_target,Iprediction_svm(:,i3),classNames);
     
    [perf_rf(1,i3),perf_rf(2,i3),...
     perf_rf(3,i3),perf_rf(4,i3),...
     perf_rf(5,i3),perf_rf(6,i3)]...
        = calc_confusionmat_measures(Test_target,Iprediction_rf(:,i3),classNames);
end

perf_svm_tab = array2table(perf_svm');
perf_svm_tab.Properties.VariableNames = {'Accuracy', 'Sensitivity','Specificity',...
                                          'BalancedAcc', 'Precision','F1score'};
perf_svm_tab.Features    = [1:1:NumFeatures]';
perf_svm_tab             = movevars(perf_svm_tab,'Features','Before','Accuracy');

perf_rf_tab = array2table(perf_rf');
perf_rf_tab.Properties.VariableNames = {'Accuracy', 'Sensitivity','Specificity',...
                                          'BalancedAcc', 'Precision','F1score'};
perf_rf_tab.Features    = [1:1:NumFeatures]';
perf_rf_tab             = movevars(perf_rf_tab,'Features','Before','Accuracy');

%%
save(fullfile(data_dir,run_dir,'finalMDL_allVars.mat'))

