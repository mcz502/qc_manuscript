function [train_scaled,test_scaled]=scale_train_test(train_data_features,test_data_features) 

    % Scale train and test data
    [p,q]=size(train_data_features);
    [p1,q1]=size(test_data_features);
    mean_train=zeros(1,q);
    std_train=zeros(1,q);
    train_scaled=zeros(p,q);
    test_scaled=zeros(p1,q1);
    for i2=1:q
        mean_train(1,i2)=mean(train_data_features(:,i2));
        std_train(1,i2)=std(train_data_features(:,i2));
        train_scaled(:,i2)=(train_data_features(:,i2)-mean_train(1,i2))./std_train(1,i2);
        test_scaled(:,i2)=(test_data_features(:,i2)-mean_train(1,i2))./std_train(1,i2);
    end
end