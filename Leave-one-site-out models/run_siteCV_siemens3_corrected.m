% Initialize variables, paths   
addpath('/vols/Data/pipelines-DPUK/QC_classifier_additionalMeasures')
addpath('/vols/Data/pipelines-DPUK/QC_classifier_additionalMeasures/Site_specific')

mydir                 = '/vols/Data/pipelines-DPUK/QC_classifier_additionalMeasures/Site_specific/adni_siemens3_asTest';
hyperparafilepath_svm = fullfile(mydir,'grSearch_svm.mat');
hyperparafilepath_rf  = fullfile(mydir,'grSearch_rf.mat');
datamat               = fullfile(mydir,'train_test_data.mat');
outpath               = fullfile(mydir,'outs_svm_rf_corrected_limitfeatures');

if ~exist(outpath,'dir')
    mkdir(outpath)
end

featurestart          = 'cjv';
ratingsCol            = 'NewManual';
siteCol               = 'Scanner';
featureset            = [10,20,30,40,50,60,70,80,90,100,104]';
runClassifier         = 1;
run_no                = 1;

% Run
siteCV_svm_rf_corrected_limitFeatures(hyperparafilepath_svm,hyperparafilepath_rf,datamat,featurestart,featureset,ratingsCol,siteCol,outpath,runClassifier,run_no)